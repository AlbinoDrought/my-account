# my-veloren

The website providing login/registering functionality for your veloren account.

The implementation of argon2 used in the browser can be found at https://github.com/xacrimon/argon2wasm.